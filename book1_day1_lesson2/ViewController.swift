//
//  ViewController.swift
//  book1_day1_lesson2
//
//  Created by 関根敦也 on 2022/11/11.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        outputLabel.text = "Hello World"
    }

    @IBOutlet weak var outputLabel: UILabel!

}

